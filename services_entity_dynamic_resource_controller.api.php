<?php
/**
 * @file
 * API documentation for Services entity dynamic resource controller module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Defines Services Entity resource decorator plugins.
 *
 * @return array
 *   An associative array with the following keys:
 *   - 'title': The display name of the controller.
 *	 - 'description': A short description of the controller.
 *   - 'class': The class name of the controller. This should be in a separate
 *     file included in the module .info file.
 *   - 'weight': Determine the weight used for this decorator when enabled in
 *     the UI.
 *
 * @see hook_services_entity_resource_decorator_info_alter()
 */
function hook_services_entity_resource_decorator_info() {
  $result = array();

  $result['clean'] = array(
    'title' => 'Clean Entity Processor',
    'description' => 'Strips out "drupalisms" such as the array structure and field_ prefixes.',
    'class' => 'ServicesEntityResourceDecoratorClean',
    'weight' => 9999,
  );

  return $result;
}

/**
 * Allow modules to alter definitions of Services Entity resource decorators.
 *
 * @param array $info
 *  The info array from hook_services_entity_resource_decorator_info().
 */
function hook_services_entity_resource_decorator_info_alter(&$info) {
}

/**
 * Determine the resource controller and decorators for the current request.
 *
 * All resource controller suggestions are aggregated; the least-weighted
 * suggestion is used to serve the request.
 *
 * All resource decorators are *combined*, each decorating the next. The last
 * decorator decorates the controller itself. Weight determines the order.
 *
 * To handle all requests for a certain entity type then you probably do not
 * want this hook (nor, indeed, this module). Instead define the 'resource
 * controller' entity info property for that entity type via, for example,
 * hook_entity_info_alter().
 *
 * Whether defining a decorator or a controller DO NOT override resourceInfo().
 * Your override will not be in place when services module's caches are built,
 * so it won't work.
 *
 * @see _services_entity_get_controller()
 * @see hook_entity_info()
 *
 * @param $server_info
 *   The loaded services server info for the current request.
 * @param $endpoint
 *   The loaded endpoint object for the current request.
 *
 * @return NULL|array
 *   Return NULL to indicate that you have nothing to say about this request.
 *   Return an array to make an override. The items of the array are:
 *
 *     - controller (array) - an array of candidate controller items (see below)
 *     - decorator  (array) - an array of multiple decorator items (see below),
 *
 *   Controller / Decorator items are keyed by class name and array-valued:
 *
 *     - class (string)    The name of the class to use.
 *     - weight (numeric)  Optional. Defaults to zero.
 */
function hook_services_entity_dynamic_resource_controller($server_info, $endpoint) {
  if ($endpoint->name == 'foobar') {
    $suggestions = array();
    // Your class must implement ServicesResourceDecoratorInterface.
    $suggestions['decorator']['MyModuleFooBarResourceDecorator'] = array(
      'class' => 'MyModuleFooBarResourceDecorator',
    );
    if (date('l') == 'Tuesday') {
      // Your class must implement ServicesResourceControllerInterface. In
      // fact, you almost certainly want to extend
      // ServicesResourceControllerAbstract as that means your resource info
      // definition remains consistent with whatever it is you are overriding.
      $suggestions['controller']['MyModuleEntityTuesdayResourceController'] = array(
        'class' => 'MyModuleEntityTuesdayResourceController',
      );
    }
    return $suggestions;
  }
}
