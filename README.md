Services entity dynamic resource controller
===========================================

Introduces a Services Entity resource controller which invokes a hook at request-time to determine which resource controller to use.

It is designed to facilitate the creation of multiple endpoints which expose the same entities but have a different behaviour to each other.


When to use this module
-----------------------
When you want to tweak the query-time behaviour of an existing operation for one endpoint but not the other, where both endpoints are exposing identical resources. For example:

1. To expose the same node data but with different API data structures:
    * /mucky/entity_node - results are structured using the Generic Entity Processor 
    * /clean/entity_node - results are structured using the Clean Entity Processor.


When NOT to use this module
---------------------------
1. Do not use this module if you simply want to take over the resource controller for a certain entity type. Instead add your class name as the 'resource controller' in its entity info via hook_entity_info_alter(). Doing this will safely override the influence of Service entity dynamic resource controller.
2. Do not use this module to override the resourceInfo() being returned. This *will not work* as the override will not be active when services builds its resource info arrays on cache clear. Instead, define your own resource via hook_services_resources().


Usage
-----

1. Install the module (as normal)
2. Go to admin/config/services/services-entity
3. Choose "Dynamically determined at request time" under "Default Entity Resource Class"
4. Under "Dynamic resource class settings" choose which resource controller to use by default.

To override the controller for a certain endpoint:

1. Edit your endpoint: visit admin/structure/services and under Operations click "Edit"
2. Under "Dynamic resource class settings" choose which resource controller to use.
  

Decorators
----------
This module introduces the concept of Resource Controller Decorators.
An arbitrary number of these may be applied to the same request and can
be used for aliasing field names or restructuring the response data.
 
We include the "Clean" decorator as part of this module. This duplicates
the functionality of the clean resource controller.


Advanced usage
--------------
Custom modules can implement a hook to suggest a resource controller using arbitrary logic. For full documentation see:

[hook_services_entity_dynamic_resource_controller()](services_entity_dynamic_resource_controller.api.php) 
