<?php
/**
 * @file
 * This resource controller invokes a hook to determine the controller to use.
 *
 * It also supports controller decorators.
 */
class ServicesEntityDynamicResourceController implements ServicesResourceControllerInterface {

  /**
   * @var ServicesResourceControllerInterface
   */
  protected $delegate;

  /**
   * Constructor.
   */
  public function __construct() {
    $server_info =  services_server_info_object();
    $endpoint_name = services_get_server_info('endpoint');
    // On non-API requests we do not support alterations.
    $decorators = array();
    if ($server_info && $endpoint_name) {
      $endpoint = services_endpoint_load($endpoint_name);
      // Invoke a hook to determine the resource controller class to use.
      // Unfortunately we cannot cache this (not even statically) as on a cold
      // cache we get called many times on the same request for different
      // endpoints. And we cannot make assumptions about the cachability of the
      // logic in implementors.
      if ($return = module_invoke_all('services_entity_dynamic_resource_controller', $server_info, $endpoint)) {
        $controllers = isset($return['controller']) ? $return['controller'] : array();
        $decorators = isset($return['decorator']) ? $return['decorator'] : array();
        uasort($controllers, 'drupal_sort_weight');
        foreach ($controllers as $controller) {
          if (class_exists($controller['class'])) {
            $controller_class = $controller['class'];
            break;
          }
        }
      }
    }
    // Fallback to the same default as services_entity.
    if (!isset($controller_class)) {
      $controller_class = variable_get('services_entity_resource_class_dynamic_fallback', 'ServicesEntityResourceController');
    }
    // Always include the default decorators.
    if ($default_decorators = variable_get('services_entity_resource_decorator_classes', array())) {
      foreach (services_entity_get_resource_decorator_info() as $decorator_info) {
        if (isset($default_decorators[$decorator_info['class']])) {
          $decorators[$decorator_info['class']] = array(
            'class' => $decorator_info['class'],
            'weight' => isset($decorator_info['weight']) ? $decorator_info['weight'] : 0,
          );
        }
      }
    }
    uasort($decorators, 'drupal_sort_weight');
    $classes = array_keys($decorators);
    // Our special last-decorator permits all others the assumption that they
    // are decorating a valid ServicesResourceDecoratorInterface.
    if (!empty($decorators)) {
      array_unshift($classes, 'ServicesEntityResourceDecoratorLast');
    }
    array_unshift($classes, $controller_class);

    // Build decoration chain.
    $controller = NULL;
    foreach ($classes as $class) {
      $decorates = isset($controller) ? $controller : NULL;
      $controller = new $class();
      if (isset($decorates)) {
        $controller->decorates($decorates);
      }
    }
    $this->delegate = $controller;
  }

  /**
   * {@inheritdoc}
   */
  public function resourceInfo($entity_type) {
    return $this->delegate->resourceInfo($entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function access($op, $args) {
    return $this->delegate->access($op, $args);
  }

  /**
   * {@inheritdoc}
   */
  public function create($entity_type, array $values) {
    $return = $this->delegate->create($entity_type, $values);
    if ($this->delegate instanceof ServicesResourceDecoratorInterface) {
      $return = $this->delegate->processResultRow($return, clone $return, $entity_type, 'create');
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function retrieve($entity_type, $entity_id, $fields, $revision) {
    $return = $this->delegate->retrieve($entity_type, $entity_id, $fields, $revision);
    if ($this->delegate instanceof ServicesResourceDecoratorInterface) {
      $return = $this->delegate->processResultRow($return, clone $return, $entity_type, 'retrieve');
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function update($entity_type, $entity_id, array $values) {
    $return = $this->delegate->update($entity_type, $entity_id, $values);
    if ($this->delegate instanceof ServicesResourceDecoratorInterface) {
      $return = $this->delegate->processResultRow($return, clone $return, $entity_type, 'update');
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function delete($entity_type, $entity_id) {
    $return = $this->delegate->delete($entity_type, $entity_id);
    if ($this->delegate instanceof ServicesResourceDecoratorInterface) {
      $return = $this->delegate->processResultRow($return, clone $return, $entity_type, 'delete');
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function index($entity_type, $fields, $parameters, $page, $pagesize, $sort, $direction) {
    $return = $this->delegate->index($entity_type, $fields, $parameters, $page, $pagesize, $sort, $direction);
    if ($this->delegate instanceof ServicesResourceDecoratorInterface) {
      foreach ($return as $key => $row) {
        $return[$key] = $this->delegate->processResultRow($row, clone $row, $entity_type, 'index');
      }
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function field($entity_type, $entity_id, $field_name, $fields = '*', $raw = FALSE) {
    $return = $this->delegate->field($entity_type, $entity_id, $field_name, $fields, $raw);
    if ($this->delegate instanceof ServicesResourceDecoratorInterface) {
      $return = $this->delegate->processResultRow($return, clone $return, $entity_type, 'field');
    }
    return $return;
  }
}
