<?php
/**
 * @file
 * Abstract resource controller decorator for aliasing fields.
 *
 * Extend this class and implement aliasValue() and deAliasValue().
 */

abstract class ServicesEntityResourceDecoratorAbstractAliaser extends ServicesEntityResourceDecoratorAbstract {

  /**
   * {@inheritdoc}
   */
  public function index($entity_type, $fields, $parameters, $page, $pagesize, $sort, $direction) {
    if ($fields !== '*') {
      $fields_array = explode(',', $fields);
      $this->deAliasValues($fields_array, $entity_type);
      $fields = implode(',', $fields_array);
    }
    $this->deAliasKeys($parameters, $entity_type);
    if (!empty($sort)) {
      $sort = $this->deAliasValue($sort, $entity_type);
    }
    return $this->decorates->index($entity_type, $fields, $parameters, $page, $pagesize, $sort, $direction);
  }

  /**
   * {@inheritdoc}
   */
  public function create($entity_type, array $values) {
    $this->deAliasKeys($values, $entity_type);
    return $this->decorates->create($entity_type, $values);
  }

  /**
   * {@inheritdoc}
   */
  public function retrieve($entity_type, $entity_id, $fields, $revision) {
    if ($fields !== '*') {
      $fields_array = explode(',', $fields);
      $this->deAliasValues($fields_array, $entity_type);
      $fields = implode(',', $fields_array);
    }
    return $this->decorates->retrieve($entity_type, $entity_id, $fields, $revision);
  }

  /**
   * {@inheritdoc}
   */
  public function update($entity_type, $entity_id, array $values) {
    $this->deAliasKeys($values, $entity_type);
    return $this->decorates->update($entity_type, $entity_id, $values);
  }

  /**
   * {@inheritdoc}
   */
  public function field($entity_type, $entity_id, $field_name, $fields = '*', $raw = FALSE) {
    if ($fields !== '*') {
      $fields_array = explode(',', $fields);
      $this->deAliasValues($fields_array, $entity_type);
      $fields = implode(',', $fields_array);
    }
    $field_name = $this->deAliasValue($field_name, $entity_type);
    return parent::field($entity_type, $entity_id, $field_name, $fields, $raw);
  }

  /**
   * {@inheritdoc}
   */
  public function processResultRow(stdClass $row, stdClass $unmodified_row, $entity_type, $operation) {
    foreach (get_object_vars($row) as $key => $value) {
      $new_key = $this->aliasValue($key, $entity_type);
      unset($row->{$key});
      $row->{$new_key} = $value;
    }
    return $this->decorates->processResultRow($row, $unmodified_row, $entity_type, $operation);
  }

  /**
   * Helper to swap keys in an array with their aliases.
   *
   * @param array $array
   *   The array to change. It is altered by reference.
   * @param string $entity_type
   *   The entity type being operated over.
   *
   * @return $this
   */
  protected function aliasKeys(array &$array, $entity_type) {
    foreach ($array as $key => $value) {
      $new_key = $this->aliasValue($key, $entity_type);
      unset($array[$key]);
      $array[$new_key] = $value;
    }
    return $this;
  }

  /**
   * Helper to swap keys in an array with their un-aliased form.
   *
   * @param array $array
   *   The array to change. It is altered by reference.
   * @param string $entity_type
   *   The entity type being operated over.
   *
   * @return $this
   */
  protected function deAliasKeys(array &$array, $entity_type) {
    foreach ($array as $key => $value) {
      $new_key = $this->deAliasValue($key, $entity_type);
      unset($array[$key]);
      $array[$new_key] = $value;
    }
    return $this;
  }

  /**
   * Helper to swap values in an array with their aliases.
   *
   * @param array $array
   *   The array to change. It is altered by reference.
   * @param string $entity_type
   *   The entity type being operated over.
   *
   * @return $this
   */
  protected function aliasValues(array &$array, $entity_type) {
    foreach ($array as $key => $value) {
      $array[$key] = $this->aliasValue($value, $entity_type);
    }
    return $this;
  }

  /**
   * Helper to swap values in an array with their un-aliased form.
   *
   * @param array $array
   *   The array to change. It is altered by reference.
   * @param string $entity_type
   *   The entity type being operated over.
   *
   * @return $this
   */
  protected function deAliasValues(&$array, $entity_type) {
    foreach ($array as $key => $value) {
      $array[$key] = $this->deAliasValue($value, $entity_type);
    }
    return $this;
  }

  /**
   * Override this method to define a mapping for field names.
   *
   * @param string $value
   *   The value to change.
   * @param string $entity_type
   *   The entity type being operated over.
   *
   * @return string
   */
  protected function aliasValue($value, $entity_type) {
    return $value;
  }

  /**
   * Override this method to define a mapping for field names.
   *
   * @param string $value
   *   The value to change.
   * @param string $entity_type
   *   The entity type being operated over.
   *
   * @return string
   */
  protected function deAliasValue($value, $entity_type) {
    return $value;
  }
}
