<?php
/**
 * @file
 * A special "last" resource controller decorator.
 */

class ServicesEntityResourceDecoratorLast extends ServicesEntityResourceDecoratorAbstract {

  /**
   * {@inheritdoc}
   */
  public function processResultRow(stdClass $row, stdClass $unmodified_row, $entity_type, $operation) {
    return $row;
  }
}
