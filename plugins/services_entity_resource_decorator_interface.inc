<?php
/**
 * @file
 * Services Entity module integration for entities.
 */

/**
 * Specifies methods for setting up resource controller decoration.
 */
interface ServicesResourceDecoratorInterface extends ServicesResourceControllerInterface  {

  /**
   * Assign the controller instance decorated by this decorator.
   *
   * @param \ServicesResourceControllerInterface $decorates
   */
  public function decorates(ServicesResourceControllerInterface $decorates);

  /**
   * Convert/alias a result row.
   *
   * End your code by invoking the same method of the object you are decorating.
   *
   * @param stdClass $row
   *   The result row data.
   * @param stdClass $unmodified_row
   *   The result row data with no alterations.
   * @param string $entity_type
   *   The entity type being operated over.
   * @param string $operation
   *   The name of the method which produced this data.
   *
   * @return stdClass
   *   The transformed data row.
   */
  public function processResultRow(stdClass $row, stdClass $unmodified_row, $entity_type, $operation);
}

