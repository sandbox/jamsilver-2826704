<?php
/**
 * @file
 * Abstract resource controller decorator.
 */

abstract class ServicesEntityResourceDecoratorAbstract implements ServicesResourceDecoratorInterface {

  /**
   * @var ServicesResourceDecoratorInterface
   */
  protected $decorates;

  /**
   * {@inheritdoc}
   */
  public function decorates(ServicesResourceControllerInterface $decorates) {
    $this->decorates = $decorates;
  }

  /**
   * {@inheritdoc}
   */
  public function resourceInfo($entity_type) {
    return $this->decorates->resourceInfo($entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function access($op, $args) {
    return $this->decorates->access($op, $args);
  }

  /**
   * {@inheritdoc}
   */
  public function create($entity_type, array $values) {
    return $this->decorates->create($entity_type, $values);
  }

  /**
   * {@inheritdoc}
   */
  public function retrieve($entity_type, $entity_id, $fields, $revision) {
    return $this->decorates->retrieve($entity_type, $entity_id, $fields, $revision);
  }

  /**
   * {@inheritdoc}
   */
  public function update($entity_type, $entity_id, array $values) {
    return $this->decorates->update($entity_type, $entity_id, $values);
  }

  /**
   * {@inheritdoc}
   */
  public function delete($entity_type, $entity_id) {
    return $this->decorates->delete($entity_type, $entity_id);
  }

  /**
   * {@inheritdoc}
   */
  public function index($entity_type, $fields, $parameters, $page, $pagesize, $sort, $direction) {
    return $this->decorates->index($entity_type, $fields, $parameters, $page, $pagesize, $sort, $direction);
  }

  /**
   * {@inheritdoc}
   */
  public function field($entity_type, $entity_id, $field_name, $fields = '*', $raw = FALSE) {
    return $this->decorates->field($entity_type, $entity_id, $field_name, $fields, $raw);
  }

  /**
   * {@inheritdoc}
   */
  public function processResultRow(stdClass $row, stdClass $unmodified_row, $entity_type, $operation) {
    return $this->decorates->processResultRow($row, $unmodified_row, $entity_type, $operation);
  }
}
